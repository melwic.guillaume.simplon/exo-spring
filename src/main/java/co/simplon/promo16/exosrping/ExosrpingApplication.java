package co.simplon.promo16.exosrping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExosrpingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExosrpingApplication.class, args);
	}

}
